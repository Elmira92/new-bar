<?php   require_once "../include/header.php";?>
 <link rel="stylesheet" type="text/css" href="css/produktet.css">
<div class="content-wrapper">
 <section class="content">
	  <div class="row">
	  	<div class="col-xs-12">
	  	 <section class="content-header">
	       <h1>Panel Produktet </h1>
	     </section>
	  	  <div class="box box-danger">
	  	   <table class="table table-bordered table-hover dataTable" >
	  	   	 <thead>
	  	   	 	<th> Nr	</th>
	  	   	 	<th> Emri	</th>
	  	   	 	<th> Kategoria	</th>
	  	   	 	<th> Nenkategoria	</th>
	  	   	 	<th> Tipi	</th>
	  	   	 	<th> Sasia	</th>
	  	   	 	<th> Cmimi	</th>
	          <th> Veprimet	</th>  	   	 
	  	   	 </thead>
	  	   	 <tbody>
	  	   	 	<tr>
	  	   	 		<td></td>
	  	   	 		<td></td>
	  	   	 		<td></td>
	  	   	 		<td></td>
	  	   	 		<td></td>
	  	   	 		<td></td>
	  	   	 		<td></td>
	  	   	 		<td>
	  	   	 		  <button class="btn btn-primary btn-m "><i class="fa white fa-pencil"></i> </button>
	              <button class="btn btn-danger btn-m "><i class="fa white fa-trash-o"></i> </button>
	  	   	 		</td>
	  	   	 	</tr>
	  	   	 </tbody>
	  	   </table>
	  	  </div><!-- perfundon box box-primary-->
	  	</div><!-- perfundon col-xs-12-->
	  </div><!-- perfundon row-->
  </section>
</div><!-- perfundon content-wrapper-->


<?php   require_once "../include/footer.php";?>