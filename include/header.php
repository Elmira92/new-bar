
<!DOCTYPE html>
<html>
 <head>
  <link rel="stylesheet" href="../css/style.css">
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Bar/Restorant</title>
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
   <link href="../bootstrap/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
   <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
   <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
   <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
   <link rel="stylesheet" href="../plugins/morris/morris.css">
   <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
   <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
   <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
   <link rel="stylesheet" href="../plugins/iCheck/flat/_all.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 </head>
 <body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
     <header class="main-header">
       <a href="#" class="logo">
         <span class="logo-lg">Bar/Restorant</b></span>
       </a>
       <nav class="navbar navbar-static-top" role="navigation"><!-- Fillojme me menune navigimit ne pjesen e siperme te faqes  -->
         <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
           <span class="sr-only">Navigimi</span>
         </a>
         <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs">Erlanda Mansaku</span>
              </a>
              <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                <p>
                  Erlanda Mansaku - Software Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="../logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
          </ul>
            
           </ul>

         </div>
       </nav><!-- Perfundojme me menune navigimit ne pjesen e siperme te faqes  -->
     </header>
     <aside class="main-sidebar"><!-- Fillojme me menune kryesore te navigimit ne pjesen majtas te faqes  -->
       <section class="sidebar">
         <div class="user-panel">
           <div class="pull-left image">
             <img id="foto_perdoruesi3" src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
           </div>
           <div class="pull-left info">
             <p> Bar/Restorant</p>
             <i class="fa fa-circle text-success"></i>
           </div>
         </div>
         <ul class="sidebar-menu">
           <li class="header">MENUJA KRYESORE</li>
           <li><a href="../panel_produktet/" >Produktet</a> </li>
           <li><a>  Magazina</a></li>
           <li><a href=""> Mallra Extra</a>  </li>
           <li><a href=""> Kontrollo fature </a> </li>
           <li><a href=""> Raportet </a> </li>
           <li><a href=""> Administro Produktet </a> </li>
         </ul>
       </section>

     </aside> <!-- Perfundojme me menune kryesore te navigimit ne pjesen majtas te faqes  -->