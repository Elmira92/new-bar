 <footer class="main-footer">
        <strong>Copyright &copy; 2016 <a href="../panel_dashboard/">Klinika Dentare</a>.</strong>
      </footer>
    <link rel="stylesheet" type="text/css" media="all" href="../plugins/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">  
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- // <script src="http://code.highcharts.com/highcharts.js" ></script> -->
    <!-- // <script src="http://code.highcharttable.org/master/jquery.highchartTable.js" ></script> -->
    
    <script>$.widget.bridge('uibutton', $.ui.button);</script>

    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../bootstrap/js/bootstrap-dialog.min.js"></script>

    <script src="../plugins/daterangepicker/moment.min.js"></script>
    <script src="../plugins/daterangepicker/daterangepicker.js"></script> 

    <script src="../bootstrap/js/bootstrap-datepicker.min.js"></script>
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

    <!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
    <!-- // <script src="../plugins/sparkline/jquery.sparkline.min.js"></script> -->
    <!-- // <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> -->
    <!-- // <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> -->
    <!-- // <script src="../plugins/knob/jquery.knob.js"></script> -->

    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <script src="../dist/js/app.min.js"></script>
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
  </body>
</html>